using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Contracts;
using System.Linq;

namespace arvoreBinaria
{
    public class arvore
    {
        public no noRaiz;
        public int contador = 0;
        public static List<int> valores = new List<int>(1000);
        

        public void inserir(int valor)
        {
            no novoNo = new no(valor);
            
           valores.Add(valor);
            no cursorAntigo = null;
            no cursor = this.noRaiz;
            
            
            novoNo.valor = valor;
            while (cursor != null)
            {
                if (novoNo.valor < cursor.valor)
                {
                    cursorAntigo = cursor;
                    cursor = cursor.Esquerda;
                }

                if (cursor != null && novoNo.valor >= cursor.valor)
                {
                    cursorAntigo = cursor;
                    cursor = cursor.Direita;
                }
                
            }

            if (valor < cursorAntigo.valor)
            {
                cursorAntigo.Esquerda = novoNo;
            }

            if (valor >= cursorAntigo.valor)
            {
                cursorAntigo.Direita = novoNo;
            }
            
        }

        public int remover(int valor)
        {
            no removido = buscar(valor);
            
            if (removido.valor == -1)
            {
                return removido.valor;
            }
            
            
            no cursorAntigo = null;
            no cursor = this.noRaiz;
          
            
            // cursor chegar no nó 'removido'
            while (cursor.valor != removido.valor)
            {
                if (valor < cursor.valor)
                {
                    //caminha para a Esquerda
                    cursorAntigo = cursor;
                    cursor = cursor.Esquerda;
                }

                if (valor > cursor.valor)
                {
                    // caminha para a Direita
                    cursorAntigo = cursor;
                    cursor = cursor.Direita;
                }

                
            }
            
            
            
            //Se for folha só remove
            if (cursor.Esquerda == null && cursor.Direita == null)
            {
                if (cursorAntigo.Esquerda == cursor)
                {
                    cursorAntigo.Esquerda = null;
                }

                if (cursorAntigo.Direita == cursor)
                {
                    cursorAntigo.Direita = null;
                }
                removido = cursor;
                return removido.valor;
            }

            //Se não existe nó à esquerda transforma o nó 'removido' no nó à sua direita
            if (cursor.Esquerda == null)
            {
                if (cursorAntigo.Esquerda == cursor)
                {
                    cursor = cursor.Direita;
                    cursorAntigo.Esquerda = cursor;
                }

                if (cursorAntigo.Direita == cursor)
                {
                    cursor = cursor.Direita;
                    cursorAntigo.Direita = cursor;
                }

                return removido.valor;
            }
            
            //Se não existe nó à esquerda e direita transforma o nó esquerdo à sua direita  -MATH

            if (cursor.Esquerda != null && cursor.Direita != null)
            {
                
                if (cursorAntigo.Esquerda == cursor)
                {
                    cursor = cursor.Esquerda;
                    cursor.Direita = removido.Direita;
                    cursorAntigo.Esquerda = cursor;
                }

                if (cursorAntigo.Direita == cursor)
                {
                    
                    cursor = cursor.Direita;
                    cursor.Esquerda = removido.Esquerda;
                    cursorAntigo.Direita = cursor;


                    
                }
                return removido.valor;
            }

            //Se existe nó à Direita
            if (cursor.Direita != null)
            {
                // caminha para a Direita
                cursorAntigo = cursor;
                cursor = cursor.Direita;
                
                while (cursor.Esquerda != null)
                {
                    // caminha para a Esquerda
                    cursorAntigo = cursor;
                    cursor = cursor.Esquerda;
                }

                no ramo = cursor;
                

                no cursorAntigoRemover = removido;
                no cursorRemover = removido;
                
                ramo.Esquerda = cursorAntigoRemover.Esquerda;
                
                cursorRemover = cursorRemover.Direita;
                while (cursorRemover != ramo)
                {
                    cursorAntigoRemover = cursorRemover;
                    cursorRemover = cursorRemover.Esquerda;
                }

                cursorAntigoRemover.Esquerda = null;

                ramo.Direita = removido.Direita;
                ramo.Esquerda = removido.Esquerda;
                
                // cursor chegar no nó 'removido'
                cursorAntigo = this.noRaiz;
                cursor = this.noRaiz;
                while (cursor.valor != removido.valor)
                {
                    if (valor < cursor.valor)
                    {
                        //caminha para a Esquerda
                        cursorAntigo = cursor;
                        cursor = cursor.Esquerda;
                    }

                    if (valor > cursor.valor)
                    {
                        // caminha para a Direita
                        cursorAntigo = cursor;
                        cursor = cursor.Direita;
                    }
                }
                //
                if (cursorAntigo.Esquerda == cursor)
                {
                    cursorAntigo.Esquerda = ramo;
                }

                if (cursorAntigo.Direita == cursor)
                {
                    cursorAntigo.Direita = ramo;
                }

                return removido.valor;
            }
            
            // IMprime o No caso o mesmo for verdadeiro
            return 0;
        }

        public no buscar(int valor)
        {
            
            no cursorAntigo = null;
            no cursor = this.noRaiz;
            

          
            while (cursor != null)
            {
                if (valor == cursor.valor)
                {
                    return cursor;
                }
                if (valor < cursor.valor)
                {
                    //caminha para a Esquerda
                    cursorAntigo = cursor;
                    cursor = cursor.Esquerda;
                    contador++;

                }

                if (valor > cursor.valor)
                {
                    // caminha para a Direita
                    cursorAntigo = cursor;
                    cursor = cursor.Direita;
                    contador++;
                }
                
            }

            return new no(-1);

        }

        public static List<int> contagem = new List<int>(1000);
        
        // public int nivelMax = contagem.Max();
        public  void Niveis()
        {
            for (int i = 0; i < valores.Count; i++)
            {
                buscar(valores[i]);
                contagem.Add(contador);
                contador = 0;
            }
        
        }

        

        // public string escrever()
        // {
        //     string diagrama = "";
        //     
        //     int nivelMax = contagem.Max();
        //     for (int i = 0; i < valores.Count; i++)
        //     {
        //         
        //         //escreva uma linha
        //     }
        //
        //     return diagrama;
        // }
        

        public arvore(int valor)
        {
            noRaiz = new no(valor);
        }
    }
    
}