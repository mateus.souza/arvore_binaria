﻿
using System;
using System.Linq;

namespace arvoreBinaria
{
    class Program
    {
        static void Main(string[] args)
        {
            arvore arvore = new arvore(30);
            arvore.inserir(10);
            arvore.inserir(25);
            arvore.inserir(5); 
            arvore.inserir(26);
            arvore.inserir(12);
            
            arvore.remover(25);

           
            Console.WriteLine("A busca retornou: " +arvore.buscar(10).valor);

            arvore.Niveis();
            int[] emOrdem = new int[arvore.valores.Count];
            for (int i = 0; i < arvore.valores.Count; i++)
            {
                emOrdem[i] = arvore.valores[i];
            }
            Array.Sort(emOrdem);
            for (int i = 0; i < emOrdem.Length; i++)
            {
                Console.WriteLine(emOrdem[i]);
            }
            
            int nivelMax = arvore.contagem.Max();
            Console.WriteLine(nivelMax);

            // Console.WriteLine("          " + arvore.noRaiz.valor);
            // Console.WriteLine("         /  \\");
            // Console.WriteLine("       " + arvore.noRaiz.Esquerda.valor + "    " + arvore.noRaiz.Direita.valor);
            // Console.WriteLine("      /  \\");
            // Console.WriteLine("     " + arvore.noRaiz.Esquerda.Esquerda.valor + "    " + arvore.noRaiz.Esquerda.Direita.valor);

            // arvore.remover(12);



        }
        
    }
}